let inputNum = parseInt(prompt("Enter a Number: "));
console.log("The number you provide is " + inputNum)

for (count = inputNum; count >= 0; count--){

	if (count % 10 === 0) {
		console.log("The number is divisible by 10. Skipping the number.")
		continue;
	}

	if (count % 5 === 0) {
		console.log(count);
	}

	if (count <= 50) {
		console.log("The current value is at 50. Terminating the loop.")
		break;
	}

}





let str = 'supercalifragilisticexpialidocious';
let vowels = 'aeiou';
let consonants = '';

for (x = 0; x < str.length; x++) {

    if (vowels.includes(str[x])) {
        continue;
    }

    else {
        consonants += str[x];
    }

}

console.log(str);
console.log(consonants);